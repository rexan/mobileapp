import 'package:flutter/material.dart';

class ConsumerDetailScreen extends StatefulWidget {
  @override
  _ConsumerDetailScreenState createState() => _ConsumerDetailScreenState();
}

class _ConsumerDetailScreenState extends State<ConsumerDetailScreen> {
  DateTime selectedDate = DateTime.now();
  TextEditingController _fromDate = new TextEditingController();
  TextEditingController _toDate = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Shiv Shakti Petroleum"),
        elevation: 0.0,
        automaticallyImplyLeading: false,
      ),
      body: Container(
        margin: EdgeInsets.all(10.0),
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 0.6)),
              child: Container(
                margin: EdgeInsets.all(10.0),
                child: Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Product'),
                        Text('Petrol'),
                        Text('Diesel'),
                      ],
                    ),
                    Spacer(),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Rs/Litre'),
                        Text('81.25'),
                        Text('71.52'),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              height: MediaQuery.of(context).size.height / 4,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black, width: 0.6),
              ),
            ),
            Row(
              children: [
// inside Widget build
                Expanded(
                  child: GestureDetector(
                    onTap: () => _selectDate(context),
                    child: AbsorbPointer(
                      child: TextFormField(
                        controller: _fromDate,
                        keyboardType: TextInputType.datetime,
                        decoration: InputDecoration(
                          hintText: 'From',
                        ),
                      ),
                    ),
                  ),
                ),

                Expanded(
                  child: GestureDetector(
                    onTap: () => _selectDate(context),
                    child: AbsorbPointer(
                      child: TextFormField(
                        controller: _toDate,
                        keyboardType: TextInputType.datetime,
                        decoration: InputDecoration(
                          hintText: 'To',
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }
}
